/*
 * Copyright (C) 2008 Stig Venaas <venaas@uninett.no>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 */

const struct protodefs *dtlsinit(uint8_t h);

/* Local Variables: */
/* c-file-style: "stroustrup" */
/* End: */
