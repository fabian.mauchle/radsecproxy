/* Copyright (C) 2011 NORDUnet A/S
 * See LICENSE for information about licensing.
 */

int radsecproxy_main(int argc, char **argv);

int main(int argc, char **argv)
{
  return radsecproxy_main(argc, argv);
}
